describe('FAQ Module Validations', () => {
    it('TC-1- Collapsible is working', () => {
      cy.visit('https://www.ta3limy.com/faq')
  
      cy.xpath("(//span[contains(@class,'Collapsible__trigger is-')])[1]")
        .click()
        
      cy.xpath("(//span[contains(@class,'Collapsible__trigger is-')])[1]")
        .should('have.attr', 'class' , 'Collapsible__trigger is-open')

      cy.xpath("(//span[contains(@class,'Collapsible__trigger is-')])[1]")
        .click()

      cy.xpath("(//span[contains(@class,'Collapsible__trigger is-')])[1]")
        .should('have.attr', 'class' , 'Collapsible__trigger is-closed')
    })


    it('TC-2- One collapsible is open at a time', () => {
        cy.visit('https://www.ta3limy.com/faq')
    
        cy.xpath("(//span[contains(@class,'Collapsible__trigger is-')])[1]")
          .click()
          
        cy.xpath("(//span[contains(@class,'Collapsible__trigger is-')])[1]")
          .should('have.attr', 'class' , 'Collapsible__trigger is-open')
  
        cy.xpath("(//span[contains(@class,'Collapsible__trigger is-')])[2]")
          .click()
  
        cy.xpath("(//span[contains(@class,'Collapsible__trigger is-')])[2]")
          .should('have.attr', 'class' , 'Collapsible__trigger is-open')
  
        cy.xpath("(//span[contains(@class,'Collapsible__trigger is-')])[1]")
          .should('have.attr', 'class' , 'Collapsible__trigger is-closed')
      })
  })

