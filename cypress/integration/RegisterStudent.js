describe('Register as student', () => {
    let studentDetails
    beforeEach( () => {
        cy.fixture('studentRegistration').then((student) => {
            studentDetails = student
        })
    })


    it('Select student', () => {
      cy.visit('https://www.ta3limy.com/register')
  
      cy.contains('طالب')
        .click()
      
    })

    it('Firstname, Lastname and Mobile', () => {    
        cy.get('[name="firstName"]')
          .type(studentDetails.firstName)
        
        cy.get('[name="lastName"]')
          .type(studentDetails.lastName)

        cy.get('[name="mobileNumber"]')
          .type(studentDetails.mobileNumber)
      })
    
      it('Gender and Class', () => {    
        cy.contains(studentDetails.gender) //أنثى
          .click()

        cy.get('[placeholder="اختر الصف الدراسي"]')
          .select(studentDetails.class)
      })

      it('Password and Confirmation', () => {    
        cy.get('[name="password"]')
          .type(studentDetails.password)

        cy.get('[name="passwordConfirmation"]')
          .type(studentDetails.passwordConfirmation)
      })

      it('Checks', () => {    
        cy.get('[for="termsAndConditionsCheck"]')
          .click()
      })

      it('Submit', () => {    
        cy.contains("تسجيل حساب")
          .click()
      })
  })